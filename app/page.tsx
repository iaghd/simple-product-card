'use client'
import React, { ChangeEvent, FormEvent } from 'react';
import styles from './page.module.css'
import { useState } from 'react'

import ProductCard from '@/components/ProductCard/ProductCard'

interface FormState {
	label: string;
	quantity: number;
	[key: string]: string | number;
}

export default function Home() {
	const maxOnStock = 20;
	const [form, setForm] = useState<FormState>({ label: '', quantity: 1 });

	// FORM HANDLE
	const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
		const { name, value } = e.target;
		let val = value;

		// FIELDS VALIDATION
		if (name === 'quantity' && val) {
			if (val > maxOnStock) val = 20;
			if (val < 1) val = 1;
		}

		setForm(s => ({ ...s, [name]: val }));
	};

	const submitHandle = (e: FormEvent<HTMLFormElement>) => {
		console.log('FILL:: ', e);
		e.preventDefault();
	}

	return (
		<main className={styles.main}>
			<ProductCard
				product={{
					picture: 'https://prod.spline.design/8SGkdz2oO9Gl0bJP/scene.splinecode',
					title: 'Yippee-Ki-Yay',
					product: 'Apple Watch',
					desc: 'Guys! I\'m eating junk and watching rubbish. You better come out and stop me!',
					inStock: 20,
					addFields: [
						{
							id: 'label',
							name: 'label',
							label: 'Custom label on the case',
							maxLength: 20,
							placeholder: 'I\'ll be back.'
						}
					]
				}}
				form={form}
				onChange={handleChange}
				onSubmit={submitHandle}
			/>
		</main>
	)
}
