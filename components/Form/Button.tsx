import { ButtonHTMLAttributes, MouseEvent, useRef, useState } from "react";
import styles from './button.module.css';

interface BtnProps extends ButtonHTMLAttributes<HTMLButtonElement> {
	label: string,
}

function Button({
	label,
	...rest
}: BtnProps) {
	const ref = useRef<HTMLButtonElement>(null);
	const [anim, setAnim] = useState(false);
	const [submit, setSubmit] = useState(false);

	// ANIMATE BACKGROUND
	const mouseMove = (e: MouseEvent<HTMLButtonElement>) => {
		if (e.target instanceof Element && ref.current) {
			// GET MOUSE POSITION
			const rect = e.target?.getBoundingClientRect();
			const x = e.clientX - rect.left;
			const y = e.clientY - rect.top;

			// SET NEW COORDINANTS FOR BACKGROUND
			ref.current.style.setProperty('--x', x + 'px');
			ref.current.style.setProperty('--y', y + 'px');
		}
	}

	// SUBMIT
	const clickHandle = (e: MouseEvent<HTMLButtonElement>) => {
		if (ref.current) {
			e.preventDefault();

			// ANIMATE BUBBLY
			setAnim(false);
			setAnim(true);
			setTimeout(function(){
				setAnim(false);
			},700);

			// CHANGE BUTTON STATE
			setSubmit(true);
			setTimeout(function(){
				setSubmit(false);
			},2000);
		}
	}

	return (
		<div className={`${styles.bubbly} ${anim && styles.animate}`}>
			<button
				ref={ref}
				className={`${styles.button} ${submit && styles.submited}`}
				onMouseMove={mouseMove}
				onClick={clickHandle}
				{...rest}
			>
				<span className="material-symbols-rounded">{submit ? 'shopping_cart' : 'add'}</span>
				<span>
					{submit ? 'In cart' : label}
				</span>
			</button>
		</div>
	)
}

export default Button;