import React, { ChangeEvent, FormEvent, InputHTMLAttributes } from 'react';
import styles from './card.module.css'
import Input from '../Form/Input';
import Button from '../Form/Button';
import Spline from '@splinetool/react-spline';

interface ExtendedInputProps extends InputHTMLAttributes<HTMLInputElement> {
	label: string;
}

interface ProductCardProps {
	product: {
		picture: string,
		title: string,
		product: string,
		desc: string,
		inStock: number
		addFields: ExtendedInputProps[]
	};
	form: {
		quantity: number;
		[key: string]: string | number;
	}
	onChange: (event: ChangeEvent<HTMLInputElement>) => void;
	onSubmit: (event: FormEvent<HTMLFormElement>) => void;
}

function ProductCard({
	product = {
		picture: 'https://prod.spline.design/8SGkdz2oO9Gl0bJP/scene.splinecode',
		title: 'Yippee-Ki-Yay',
		product: 'Apple Watch',
		desc: 'Guys! I\'m eating junk and watching rubbish. You better come out and stop me!',
		inStock: 20,
		addFields: []
	},
	form = {
		quantity: 1
	},
	onChange,
	onSubmit
}: ProductCardProps) {

	// GENERATE ADDITIONAL FIELDS
	const addFields = [];
	for (const field of product.addFields) {
		const id = field.id || '';
		addFields.push(
			<Input
				key={id}
				value={form[id]}
				onChange={onChange}
				{...field}
			/>
		);
	}

	return (
		<div className={styles.card}>
			{/* IMAGE AREA */}
			<div className={styles.img}>
				<Spline scene={product.picture}/>
				<div className={styles.desc}>
					<span className="material-symbols-rounded">pan_tool</span>
					Grab to rotate
				</div>
			</div>
			{/* FORM AREA */}
			<form onSubmit={onSubmit} className={styles.form}>
				{/* INFO */}
				<div className={styles.info}>
					<h1>{product.title}</h1>
					<h2>{product.product}</h2>
					<p>{product.desc}</p>
				</div>
				{/* INPUTS */}
				{addFields}
				<Input
					type='number'
					id='quantity'
					label='Quantity*'
					inputMode="numeric"
					required
					value={form.quantity}
					onChange={onChange}
					min={1}
					max={product.inStock}
				/>
				{/* SUBMIT */}
				<Button type='submit' label='Add to card' />
			</form>
		</div>
	);
}

export default ProductCard;
