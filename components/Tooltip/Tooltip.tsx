import { AnimatePresence, motion } from "framer-motion"
import styles from './tooltip.module.css';

interface TooltipProps {
	icon: string;
	title: string;
}

function Tooltip ({
	icon = 'priority_high',
	title
}: TooltipProps) {

	return (
		<AnimatePresence>
			{title &&
				<motion.div
					initial={{
						opacity: 0,
						transform: 'translateY(calc(100% - 0.5rem))'
					}}
					animate={{
						opacity: 1,
						transform: 'translateY(calc(100% + 0.5rem))'
					}}
					exit={{
						opacity: 0,
						transform: 'translateY(calc(100% - 0.5rem))'
					}}
					transition={{duration: 0.2}}
					className={styles.tooltip}
				>
					<span className="material-symbols-rounded">{icon}</span>
					{title}
				</motion.div>
			}
		</AnimatePresence>
	)
}

export default Tooltip;