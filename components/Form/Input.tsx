import { ChangeEvent, InputHTMLAttributes, useState } from "react";
import styles from './input.module.css';
import Tooltip from '../Tooltip/Tooltip';

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
	label: string;
	onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

function Input({
	id,
	label,
	onChange,
	...rest
}: InputProps) {
	const [limit, setLimit] = useState('');

	// DELAY ANIMATION
	const delayedAnim = () => setTimeout(function(){
		setLimit('');
	},2500);

	// ON CHANGE
	const changeHandle = (e: ChangeEvent<HTMLInputElement>) => {
		const {value, maxLength, max} = e.target;

		// INPUT REACHED MAX LENGTH
		if (maxLength > 0 && value.length >= maxLength) {
			setLimit('You won\'t get any more letters. Be brief.');
			delayedAnim();
		} else if (max && Number(value) >= Number(max)) {
			// INPUT REACHED MAXIMUM NUMBER
			setLimit('Don\'t be greedy, that\'s all we have in stock.');
			delayedAnim();
		} else {
			setLimit('');
		}

		onChange(e);
	}

	return (
		<div className={styles.inputWrapper}>
			<label htmlFor={id} className={styles.inputLabel}>{label}</label>
			<input
				className={styles.inputInput}
				id={id}
				name={id}
				onChange={changeHandle}
				{...rest}
			/>
			<Tooltip
				title={limit}
				icon={'priority_high'}
			/>
		</div>
	)
}

export default Input;